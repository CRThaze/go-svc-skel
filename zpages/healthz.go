package zpages

import (
	"encoding/json"
	"net/http"

	"github.com/rs/zerolog/log"
)

// func ageTest(last int64) (eval bool, res string) {
// 	age := metrics.ComputeAge(last)
// 	res = fmt.Sprint(age)
// 	eval = age < float64(c.Settings.TickIntervalSecs)*c.Settings.TickDelayToleranceFactor
// 	return
// }

var testTable = []struct {
	name string
	fn   func() (bool, string)
}{
	// {
	// 	name: "last_probe_age",
	// 	fn: func() (bool, string) {
	// 		return ageTest(metrics.LastProbeTime.Load())
	// 	},
	// },
	// {
	// 	name: "last_scrape_age",
	// 	fn: func() (bool, string) {
	// 		return ageTest(metrics.LastScrapeTime.Load())
	// 	},
	// },
	// {
	// 	name: "last_ship_age",
	// 	fn: func() (bool, string) {
	// 		return ageTest(metrics.LastShipTime.Load())
	// 	},
	// },
}

type healthHandler struct{}

func (h *healthHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	everythingOK := true
	resultsTable := map[string]string{}
	for _, test := range testTable {
		var ok bool
		ok, resultsTable[test.name] = test.fn()
		if !ok {
			everythingOK = false
		}
	}
	if !everythingOK {
		w.WriteHeader(http.StatusInternalServerError)
	}
	data, err := json.MarshalIndent(&map[string]interface{}{
		"healthy": everythingOK,
		"results": resultsTable,
	}, "", "\t")
	if err != nil {
		log.Error().Err(err).Msg("Failed to render healthz response")
	}
	w.Write(data)
}
