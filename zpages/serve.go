package zpages

import (
	"context"
	"fmt"
	"net/http"
	"sync"

	c "github.com/comet-ml/comet-o11y/config"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog/log"
)

func Serve(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()
	zSrv := http.Server{Addr: fmt.Sprintf(":%d", c.Settings.ZPagePort)}
	http.Handle("/varz", promhttp.Handler())
	http.Handle("/healthz", &healthHandler{})

	go func() {
		log.Info().Msgf("Serving zPages on: 0.0.0.0:%d", c.Settings.ZPagePort)
		if err := zSrv.ListenAndServe(); err != nil {
			log.Warn().Err(err).Send()
		}
	}()
	<-ctx.Done()
	log.Info().Msg("Shutting down zPages HTTP server")
	zSrv.Shutdown(context.Background())
}
