package config

import (
	"flag"
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

const (
	defaultMetricsPrefix = "comet"
	defaultZPagePort     = 9091
)

var Settings struct {
	ZPagePort      uint16
	ConsoleLogging bool
	LogLevel       string
}

var flags = struct {
	zPagePort      uint64
	consoleLogging *bool
	logLevel       *string
}{
	consoleLogging: &Settings.ConsoleLogging,
	logLevel:       &Settings.LogLevel,
}

func init() {
	// Setup Command Line Arguments.
	flag.Uint64Var(
		&flags.zPagePort,
		"zPagePort",
		defaultZPagePort,
		"The port on which to serve zpages for this process.",
	)
	flag.BoolVar(
		flags.consoleLogging,
		"consoleLogging",
		false,
		"Log in a more human readable format.",
	)
	flag.StringVar(
		flags.logLevel,
		"logLevel",
		"info",
		"Verbosity of logging. Can be: trace, debug, info, warn, error, fatal, panic.",
	)
	flag.Parse()

	// Populate Config.
	Settings.ZPagePort = uint16(flags.zPagePort)

	// Setup Logger.
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	if Settings.ConsoleLogging {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	}

	if logLevel, err := zerolog.ParseLevel(Settings.LogLevel); err != nil {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
		log.Warn().Msgf("Invalid logging level provided (%s); defaulting to INFO", flags.logLevel)
	} else {
		zerolog.SetGlobalLevel(logLevel)
	}
}

func Validate() {
	// Validate Config.
	if uint64(Settings.ZPagePort) != flags.zPagePort {
		log.Fatal().Msgf("Invalid Port Number for Prometheus provied (%d)", flags.zPagePort)
	}
}
