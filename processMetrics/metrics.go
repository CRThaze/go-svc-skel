package processMetrics

import (
	c "github.com/comet-ml/comet-o11y/config"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/zerolog/log"
)

var (
// LastScrapeTime,
// LastProbeTime,
// LastShipTime atomic.Int64
)

func init() {
	// LastScrapeTime.Store(-1)
	// LastProbeTime.Store(-1)
	// LastShipTime.Store(-1)
}

var (
	FailingProbes = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: c.Settings.InternalMetricsPrefix,
		Name:      "failing_probes_count",
		Help:      "Count of probes currently failing.",
	})
	FailingScrapes = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: c.Settings.InternalMetricsPrefix,
		Name:      "failing_scrapes_count",
		Help:      "Count of scrapes currently failing.",
	})
	// LastScrapeAge = prometheus.NewGaugeFunc(
	// 	prometheus.GaugeOpts{
	// 		Namespace: c.Settings.InternalMetricsPrefix,
	// 		Name:      "last_scrape_age_seconds",
	// 		Help:      "Seconds since last scrape started.",
	// 	},
	// 	func() float64 { return ComputeAge(LastScrapeTime.Load()) },
	// )
	// LastProbeAge = prometheus.NewGaugeFunc(
	// 	prometheus.GaugeOpts{
	// 		Namespace: c.Settings.InternalMetricsPrefix,
	// 		Name:      "last_probe_age_seconds",
	// 		Help:      "Seconds since last scrape started.",
	// 	},
	// 	func() float64 { return ComputeAge(LastProbeTime.Load()) },
	// )
	// LastShipAge = prometheus.NewGaugeFunc(
	// 	prometheus.GaugeOpts{
	// 		Namespace: c.Settings.InternalMetricsPrefix,
	// 		Name:      "last_ship_age_seconds",
	// 		Help:      "Seconds since last shipping started.",
	// 	},
	// 	func() float64 { return ComputeAge(LastShipTime.Load()) },
	// )
)

var metrics = []prometheus.Collector{
	FailingProbes,
	FailingScrapes,
	// LastScrapeAge,
	// LastProbeAge,
	// LastShipAge,
}

func Register() {
	var registrationErrors bool
	failedMetricDescriptions := []string{}
	for _, col := range metrics {
		if err := prometheus.Register(col); err != nil {
			d := make(chan *prometheus.Desc)
			go col.Describe(d)
			for desc := range d {
				failedMetricDescriptions = append(
					failedMetricDescriptions,
					desc.String(),
				)
			}
			registrationErrors = true
		}
	}
	if registrationErrors {
		log.Fatal().Strs(
			"failed_metric_descs", failedMetricDescriptions,
		).Msgf("Failed to register some or all prometheus metrics")
	}
}
