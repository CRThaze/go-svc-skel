package main

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"syscall"

	metrics "github.com/comet-ml/comet-o11y/processMetrics"
	"github.com/comet-ml/comet-o11y/zpages"

	"github.com/rs/zerolog/log"
)

func init() {
	config.Validate()
	metrics.Register()
}

func main() {
	log.Debug().Msgf("%+v\n", config.Settings)

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	var wg sync.WaitGroup

	go func() {
		termSigs := make(chan os.Signal, 1)
		signal.Notify(termSigs, syscall.SIGINT, syscall.SIGTERM)
		<-termSigs
		cancel()
	}()

	wg.Add(1)
	go zpages.Serve(ctx, &wg)

	wg.Wait()
}
